/**
 * External dependencies
 */
const path = require( 'path' );

/**
 * WordPress dependencies
 */
const defaultConfig = require( '@wordpress/scripts/config/webpack.config' );

module.exports = {
	...defaultConfig,
	entry: {
		editor: './assets/js/src/block/ampstatistics/index.js',
	},
	output: {
		path: path.resolve( __dirname, 'assets/js/dist/block/ampstatistics' ),
		filename: '[name].js',
	},
};
