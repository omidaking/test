<?php
/**
 * Tests for Router class.
 *
 * @package BlockScaffolding
 */

namespace XWP\BlockScaffolding;

use Mockery;
use WP_Mock;

/**
 * Tests for the Router class.
 */
class TestRouter extends TestCase {

	/**
	 * Test init.
	 *
	 * @covers \XWP\BlockScaffolding\Router::init()
	 * @covers \XWP\BlockScaffolding\Router::routes()
	 * @covers \XWP\BlockScaffolding\Router::modules_routes()
	 */
	public function test_init() {
		$router = new Router( Mockery::mock( Plugin::class ) );

		WP_Mock::expectFilter( 'block-scaffolding/add/routes', [] );

		$this->assertEquals( [], $router->modules_routes() );

		WP_Mock::expectFilterAdded( 'block-scaffolding/add/routes', [ $router, 'routes' ], 10, 1 );

		$router->init();

		$router->routes( [] );
	}


}
