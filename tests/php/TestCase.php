<?php
/**
 * Tests Case class.
 *
 * @package BlockScaffolding
 */
namespace XWP\BlockScaffolding;

use Mockery;
use WP_Mock;
use XWP\BlockScaffolding\Router;
use XWP\BlockScaffolding\Plugin;

/**
 * Tests for the Router class.
 */
class TestCase extends WP_Mock\Tools\TestCase {

	use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

	public function setUp(): void {
		parent::setUp();
		WP_Mock::setUp();
	}

	public function tearDown(): void {
		WP_Mock::tearDown();
		Mockery::close();
		parent::tearDown();
	}

	public function write_log( $log ) {
		fwrite( STDERR, print_r( $log, true ) );
	}

}
