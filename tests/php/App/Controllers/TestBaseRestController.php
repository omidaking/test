<?php
/**
 * Tests for rest controller class.
 *
 * @package BlockScaffolding
 */

namespace XWP\BlockScaffolding\App\Services;

use Mockery;
use WP_Mock;
use XWP\BlockScaffolding\Plugin;
use XWP\BlockScaffolding\App\Controllers\BaseRestController;
use XWP\BlockScaffolding\TestCase;

/**
 * Test rest class.
 *
 * @sincd 1.0.0
 */
class TestBaseRestController extends TestCase {

	/**
	 * Plugin instance.
	 *
	 * @var string
	 */
	protected $plugin;

	public function setUp() : void {
		parent::setUp();
		Mockery::mock( 'WP_REST_Controller' );
		Mockery::namedMock( 'WP_REST_Server', FetcherStub::class )
		->shouldReceive( 'READABLE' )
		->andReturn( 'GET' );
		$this->plugin = Mockery::mock( Plugin::class );
	}

	/**
	 * Test init.
	 *
	 * @covers \XWP\BlockScaffolding\App\Controllers\BaseRestController::run()
	 * @covers \XWP\BlockScaffolding\App\Controllers\BaseRestController::register_hooks()
	 * @covers \XWP\BlockScaffolding\App\Controllers\BaseRestController::register_route()
	 * @covers \XWP\BlockScaffolding\App\Controllers\BaseRestController::get_namespace()
	 */
	public function test_run() {
		WP_Mock::userFunction( 'register_rest_route' )->once();

		$restcontroller = BaseRestController::get_instance( $this->plugin, false, false );

		WP_Mock::expectActionAdded( 'rest_api_init', array( $restcontroller, 'register_route' ) );

		$restcontroller->run();

		$restcontroller->register_hooks();

		$restcontroller->register_route();
	}

}

class FetcherStub {
	const READABLE = 'GET';
}
