<?php
/**
 * Tests for block class.
 *
 * @package BlockScaffolding
 */

namespace XWP\BlockScaffolding\App\Controllers;

use Mockery;
use WP_Mock;
use XWP\BlockScaffolding\Plugin;
use XWP\BlockScaffolding\App\Controllers\Block;
use XWP\BlockScaffolding\App\Views\AMPStatistics as View;
use XWP\BlockScaffolding\TestCase;

/**
 * Tests for the block class.
 */
class TestBlock extends TestCase {

	/**
	 * Test init.
	 *
	 * @covers \XWP\BlockScaffolding\App\Controllers\Block::run()
	 * @covers \XWP\BlockScaffolding\App\Controllers\Block::register_hooks()
	 * @covers \XWP\BlockScaffolding\App\Controllers\Block::register_blocks()
	 * @covers \XWP\BlockScaffolding\App\Controllers\Block::register_block()
	 * @covers \XWP\BlockScaffolding\App\Controllers\BaseBlockController::register()
	 */
	public function test_run() {
		// each of these methods get a test on the child class
		$plugin = Mockery::mock( Plugin::class );

		$plugin->shouldReceive( 'asset_url' )->once();

		$plugin->shouldReceive( 'asset_version' )->once();

		WP_Mock::userFunction( 'wp_enqueue_script' )->once();

		WP_Mock::userFunction( 'register_block_type' )->once();

		$block = Block::get_instance( $plugin, false, new View );

		WP_Mock::expectActionAdded( 'init', array( $block, 'register_blocks' ) );

		$block->run();

		$block->register_hooks();

		$block->register_blocks();
	}

}
