<?php
/**
 * Tests for AMPStatistics class.
 *
 * @package BlockScaffolding
 */

namespace XWP\BlockScaffolding\App\Components\Blockeditor;

use Mockery;
use WP_Mock;
use XWP\BlockScaffolding\App\Controllers\Block as BlockController;
use XWP\BlockScaffolding\App\Views\AMPStatistics as View;
use XWP\BlockScaffolding\App\Components\Blockeditor\AMPStatistics;
use XWP\BlockScaffolding\TestCase;
use XWP\BlockScaffolding\Plugin;

/**
 * Tests for the AMPStatic block class.
 */
class TestAMPStatistics extends TestCase {


	/**
	 * Test register_script.
	 *
	 * @covers \XWP\BlockScaffolding\App\Components\Blockeditor\AMPStatistics::register_script()
	 */
	public function test_register_script() {
		$plugin = Mockery::mock( Plugin::class );

		$plugin->shouldReceive( 'asset_url' )
		->once()
		->with( 'assets/js/dist/block/ampstatistics/editor.js' )
		->andReturn( 'http://example.com/assets/js/dist/block/ampstatistics/editor.js' );

		$plugin->shouldReceive( 'asset_version' )
		->once()
		->andReturn( '1.0.0' );

		WP_Mock::userFunction( 'wp_enqueue_script' )
		->once()
		->with(
			'ampstatistics-script',
			'http://example.com/assets/js/dist/block/ampstatistics/editor.js',
			Mockery::type( 'array' ),
			'1.0.0',
			true
		);

		$instance = AMPStatistics::get_instance( $plugin, false, false );

		$instance->register_script();
	}

}
