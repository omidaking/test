<?php
/**
 * Tests for ampstatics rest class.
 *
 * @package BlockScaffolding
 */

namespace XWP\BlockScaffolding\App\Services;

use Mockery;
use WP_Mock;
use XWP\BlockScaffolding\Plugin;
use XWP\BlockScaffolding\App\Services\AMPStatisticsRestController;
use XWP\BlockScaffolding\App\Services\TestBaseRestController;


/**
 * Test rest class.
 *
 * @sincd 1.0.0
 */
class TestAMPStatisticsRestController extends TestBaseRestController {


	public function setUp() : void {
		parent::setUp();
	}

	/**
	 * Test to see correct instance of rest class
	 *
	 * @covers \XWP\BlockScaffolding\App\Services\AMPStatisticsRestController::get_instance()
	 */
	public function test_get_instance() {
		$instance = AMPStatisticsRestController::get_instance( $this->plugin, false, false );
		$this->assertTrue( $instance instanceof AMPStatisticsRestController );
	}

	/**
	 * Test register rest route method on register
	 *
	 * @covers \XWP\BlockScaffolding\App\Services\AMPStatisticsRestController::register()
	 */
	public function test_register() {
		WP_Mock::userFunction(
			'register_rest_route',
			array(
				'times'  => 1,
				'return' => true,
			)
		);

		$instance = AMPStatisticsRestController::get_instance( $this->plugin, false, false );
		$instance->register();
	}

	/**
	 * Check return predifined value
	 *
	 * @covers \XWP\BlockScaffolding\App\Services\AMPStatisticsRestController::get_item_schema()
	 */
	public function test_get_item_schema() {
		$instance = AMPStatisticsRestController::get_instance( $this->plugin, false, false );

		$this->assertEquals(
			[
				'$schema'    => 'http://json-schema.org/draft-04/schema#',
				'title'      => 'amp-statistics',
				'type'       => 'object',
				'properties' => [
					'validation_urls' => [
						'type'     => 'integer',
						'readonly' => true,
					],
					'errors'          => [
						'type'     => 'integer',
						'readonly' => true,
					],
					'theme_support'   => [
						'type'     => 'string',
						'readonly' => true,
					],
				],
			],
			$instance->get_item_schema()
		);

	}

}
