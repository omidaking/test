<?php
/**
 * Plugin Name: Block Scaffolding
 * Description: Block Scaffolding for WordPress.
 * Version: 1.0.0
 * Author: XWP
 * Author URI: https://github.com/xwp/block-scaffolding-wp
 * Text Domain: block-scaffolding-wp-omid
 *
 * @package BlockScaffolding
 */

namespace XWP\BlockScaffolding;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Support for site-level autoloading.
if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
	require_once __DIR__ . '/vendor/autoload.php';
}

/**
 *  Initial class
 */
class BlockScaffoldingInitial {
	/**
	 *  Php version required
	 */
	const  PHP_VERSION = '7.1';

	/**
	 * Wp Version required
	 */
	const  REQUIRED_WP_VERSION = '5.6';

	/**
	 * Error container
	 *
	 * @since 1.0.0
	 *
	 * @var array contain error.
	 */
	public $error = array();

	/**
	 * The single instance of the class.
	 *
	 * @var instance
	 * @since 1.0.0
	 */
	protected static $instance = null;

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheating huh?', 'block-scaffolding-wp-omid' ), '1.0.0' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheating huh?', 'block-scaffolding-wp-omid' ), '1.0.0' );
	}

	/**
	 * Main BlockScaffolding Instance.
	 *
	 * Ensures only one instance of BlockScaffolding is loaded or can be loaded.
	 *
	 * @return BlockScaffolding - Main instance.
	 * @since 1.0.0
	 */
	public static function instance() {
		// Get an instance of Class
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		// Return the instance
		return self::$instance;
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		// Check requirements and load main class
		if ( $this->requirements_needs() ) {
			// Run the plugin
			$this->_run();
		} else {
			add_action( 'admin_notices', array( $this, 'requirements_error' ) );
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			// Unset activated notice
			unset( $_GET['activate'] );
			// Set deactivated notice
			$_GET['deactivate'] = true;
			deactivate_plugins( plugin_basename( __FILE__ ) );
		}
	}

	/**
	 * Begins execution of the plugin
	 *
	 * @since    1.0.0
	 */
	private function _run() {
		$router = new Router( new Plugin( __FILE__ ) );
		add_action( 'plugins_loaded', [ $router, 'init' ] );

		register_activation_hook( __FILE__, array( new App\Bootstrap\Activator(), 'activate' ) );
		register_deactivation_hook( __FILE__, array( new App\Bootstrap\Deactivator(), 'deactivate' ) );
	}

	/**
	 * Checks if the system requirements are met
	 *
	 * @since    1.0.0
	 * @return bool True if system requirements are met, false if not
	 */
	public function requirements_needs() {
		global $wp_version;

		if ( version_compare( self::PHP_VERSION, PHP_VERSION, '>' ) ) {
			$this->error[] = 'php_version';
			return false;
		}

		if ( version_compare( $wp_version, self::REQUIRED_WP_VERSION, '<' ) ) {
			$this->error[] = 'wp_version';
			return false;
		}

		if ( ! defined( 'AMP__VERSION' ) ) {
			$this->error[] = 'amp';
			return false;
		}

		return true;
	}

	/**
	 * Prints an error that the system requirements weren't met.
	 *
	 * @since    1.0.0
	 */
	public function requirements_error() {
		global $wp_version;
		require_once wp_normalize_path( plugin_dir_path( __FILE__ ) ) . '/php/App/Templates/errors/requirements-error.php';
	}

}

// Fire :)
BlockScaffoldingInitial::instance();
