<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding;

/**
 * Plugin Router.
 */
final class Router {

	/**
	 * Use this route type if a controller/model needs to be loaded on every
	 * request
	 *
	 * @since    1.0.0
	 */
	const ANY = 'any';

	/**
	 * Use this route type if a controller/model needs to be loaded only on
	 * admin/dashboard request
	 *
	 * @since    1.0.0
	 */
	const ADMIN = 'admin';

	/**
	 * Use this route type if a controller/model needs to be loaded only on
	 * admin/dashboard & it has ajax support
	 *
	 * @since    1.0.0
	 */
	const ADMIN_WITH_POSSIBLE_AJAX = 'admin_with_possible_ajax';

	/**
	 * Use this route type if a controller/model needs to be loaded only on
	 * Frontend
	 *
	 * @since    1.0.0
	 */
	const FRONTEND = 'frontend';

	/**
	 * Use this route type if a controller/model needs to be loaded only on
	 * Frontend & it has ajax support
	 *
	 * @since    1.0.0
	 */
	const FRONTEND_WITH_POSSIBLE_AJAX = 'frontend_with_possible_ajax';

	/**
	 * Holds Model, View & Controllers triad for All routes except 'Model Only' Routes.
	 *
	 * @var array
	 * @since    1.0.0
	 */
	private static $modules = null;

	/**
	 * Plugin interface.
	 *
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * Setup the plugin instance.
	 *
	 * @param Plugin $plugin Instance of the plugin abstraction.
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 * Hook into WP.
	 *
	 * @return void
	 */
	public function init() {
		add_filter( 'block-scaffolding/add/routes', [ $this, 'routes' ] );
		$this->_register_routes();
	}

	/**
	 * Attach routes to load for plugin
	 *
	 * @return routes
	 * @since    1.0.0
	 */
	function routes( $routes ) {
		$routes[] = array(
			'type'       => 'any',
			'controller' => 'XWP\BlockScaffolding\App\Controllers\Block@run',
			'model'      => false,
			'view'       => 'XWP\BlockScaffolding\App\Views\AMPStatistics',
		);

		$routes[] = array(
			'type'       => 'any',
			'controller' => 'XWP\BlockScaffolding\App\Controllers\BaseRestController@run',
			'model'      => false,
			'view'       => false,
		);

		return $routes;
	}

	/**
	 * Register modules args.
	 *
	 * @return mixed|null|void
	 */
	public static function modules_routes() {

		if ( ! is_null( self::$modules ) ) {
			return self::$modules;
		}

		return self::$modules = apply_filters( 'block-scaffolding/add/routes', array() );
	}

	/**
	 * Registers Enqueued Routes
	 *
	 * @return void
	 * @since    1.0.0
	 */
	private function _register_routes() {
		// Compontents
		$modules = self::modules_routes();

		foreach ( $modules as $mvc_component ) {
			if ( $this->_is_request( $mvc_component['type'] ) && ! empty( $mvc_component['type'] ) && trim( $mvc_component['controller'] ) ) {
				$this->_dispatch( $mvc_component );
			}
		}
	}

	/**
	 * Dispatches the route of specified $route_type by creating a controller object
	 *
	 * @param array  $mvc_component Model-View-Controller triads for all registered routes.
	 * @param string $route_type Route Type.
	 * @return void
	 * @since    1.0.0
	 */
	private function _dispatch( $mvc_component ) {
		$model = false;
		$view  = false;

		if ( isset( $mvc_component['controller'] ) && false === $mvc_component['controller'] ) {
			return;
		}

		if ( is_callable( $mvc_component['controller'] ) ) {
			$mvc_component['controller'] = call_user_func( $mvc_component['controller'] );

			if ( false === $mvc_component['controller'] ) {
				return;
			}
		}

		if ( isset( $mvc_component['model'] ) && false !== $mvc_component['model'] ) {
			if ( is_callable( $mvc_component['model'] ) ) {
				$mvc_component['model'] = call_user_func( $mvc_component['model'] );
			}

			$model = $mvc_component['model'];
		}

		if ( isset( $mvc_component['view'] ) && false !== $mvc_component['view'] ) {
			if ( is_callable( $mvc_component['view'] ) ) {
				$mvc_component['view'] = call_user_func( $mvc_component['view'] );
			}

			$view = $mvc_component['view'];
		}

		$action   = null;
		$segments = explode( '@', $mvc_component['controller'] );
		if ( count( $segments ) === 1 ) {
			$controller = $segments[0];
		} else {
			list($controller, $action) = $segments;
		}

		$controller_instance = $controller::get_instance( $this->plugin, $model, $view );
		if ( null !== $action ) {
			$controller_instance->$action();
		}
	}

	/**
	 * Identifies Request Type.
	 *
	 * @param string $route_type Route Type to identify.
	 * @return boolean
	 * @since    1.0.0
	 */
	private function _is_request( $route_type ) {
		switch ( $route_type ) {
			case self::ANY:
				return true;

			case self::ADMIN:
			case self::ADMIN_WITH_POSSIBLE_AJAX:
				return is_admin();

			case self::FRONTEND:
			case self::FRONTEND_WITH_POSSIBLE_AJAX:
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' ) && ! defined( 'REST_REQUEST' );
		}
	}
}
