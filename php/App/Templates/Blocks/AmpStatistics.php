<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
?>
<div <?php echo get_block_wrapper_attributes( [ 'class' => 'amp-statistics-wrapper' ] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>>
	<div class="post-counts-container">
		<h3 role="presentation">
			<?php esc_html_e( 'AMP Validation Statistics', 'site-counts' ); ?>
		</h3>
		<ul>
			<li>
				<?php
				echo sprintf(
					/* translators: %d: validated counts */
					esc_html__( 'There are %1$d validatet URL.', 'block-scaffolding-wp-omid' ),
					absint( $statistics['validated_urls'] )
				);
				?>
			</li>
			<li>
				<?php
				echo sprintf(
					/* translators: %d: validation error */
					esc_html__( 'There are %1$d validation errors.', 'block-scaffolding-wp-omid' ),
					absint( $statistics['errors'] )
				);
				?>
			</li>
			<?php if ( $showAMPAdditionalStatistics ) : ?>
				<li>
					<?php
					echo sprintf(
						/* translators: %s: AMP theme support mode */
						esc_html__( 'The template mode is %s.', 'block-scaffolding-wp-omid' ),
						esc_html( $statistics['theme_support'] )
					);
					?>
				</li>
			<?php endif; ?>
		</ul>
	</div>
</div>
