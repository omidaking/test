<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
?>	
<div class="error notice is-dismissible">
	<p>
		<?php esc_html_e( 'Block Scaffolding Error: Your environment doesn\'t meet all of the system requirements listed below.', 'block-scaffolding-wp-omid' ); ?>
	</p>
	<ul class="ul-disc">
		<?php if ( in_array( 'php_version', $this->error ) ) : ?>
		<li>
			<strong>
				<?php esc_html_e( 'PHP 7.1+ is required', 'block-scaffolding-wp-omid' ); ?>
			</strong>
			<em>
				<?php esc_html_e( 'You\'re running version ', 'block-scaffolding-wp-omid' ); ?>
				<?php echo PHP_VERSION; ?>
			</em>
		</li>
		<?php endif; ?>

		<?php if ( in_array( 'wp_version', $this->error ) ) : ?>
		<li>
			<strong>
				<?php esc_html_e( 'WordPress 5.6+ is required', 'block-scaffolding-wp-omid' ); ?>
			</strong>
			<em>
				<?php esc_html_e( 'You\'re running version ', 'block-scaffolding-wp-omid' ); ?>
				<?php echo esc_html( $wp_version ); ?>
			</em>
		</li>
		<?php endif; ?>

		<?php if ( in_array( 'amp', $this->error ) ) : ?>
		<li>
			<strong>
				<?php esc_html_e( 'AMP plugin is not activated.', 'block-scaffolding-wp-omid' ); ?>
			</strong>
			<em>
				<?php esc_html_e( 'You need to activate AMP plugin first, To access AMP statistics block.', 'block-scaffolding-wp-omid' ); ?>
			</em>
		</li>
		<?php endif; ?>
	</ul>
</div>
