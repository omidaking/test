<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Services;

use XWP\BlockScaffolding\App\Controllers\BaseRestController;
use XWP\BlockScaffolding\App\Interfaces\Registerable;
use XWP\BlockScaffolding\App\Traits\AMPStatistics;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;
use WP_REST_Server;

/**
 * Rest endpoint for fetching the total count of unreviewed validation URLs and validation errors,
 * This class is edited version of amp plugin validationcount class api to make it public for all users
 * To display in our block not only for admin user, Also fetched theme_support option from option table
 * To display it in our block trough one route.
 *
 * @since 1.0.0
 * @internal
 */
final class AMPStatisticsRestController extends BaseRestController implements Registerable {

	use AMPStatistics;

	/**
	 * Registers all routes for the controller.
	 */
	public function register() {
		register_rest_route(
			$this->get_namespace(),
			'/amp-statistics',
			[
				[
					'methods'             => WP_REST_Server::READABLE,
					'callback'            => [ $this, 'get_items' ],
					'args'                => [],
					'permission_callback' => '__return_true',
				],
				'schema' => [ $this, 'get_item_schema' ],
			]
		);
	}

	/**
	 * Retrieves the block type' schema, conforming to JSON Schema.
	 *
	 * @return array Item schema data.
	 */
	public function get_item_schema() {
		return [
			'$schema'    => 'http://json-schema.org/draft-04/schema#',
			'title'      => 'amp-statistics',
			'type'       => 'object',
			'properties' => [
				'validation_urls' => [
					'type'     => 'integer',
					'readonly' => true,
				],
				'errors'          => [
					'type'     => 'integer',
					'readonly' => true,
				],
				'theme_support'   => [
					'type'     => 'string',
					'readonly' => true,
				],
			],
		];
	}
}
