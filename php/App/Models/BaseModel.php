<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Models;

use XWP\BlockScaffolding\App\Core\Model;

/**
 * Abstract For Frontside Related Models.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Models
 */
abstract class BaseModel {
	use Model;
}
