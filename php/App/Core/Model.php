<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Core;

/**
 * Abstract class to define base methods for model classes.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Core
 */
trait Model {

	use Base;

	/**
	 * Provides access to a single instance of a module.
	 *
	 * @since    1.0.0
	 * @return object
	 */
	public static function get_instance() {
		$classname = get_called_class();
		$instance  = self::get( $classname );

		if ( null === $instance ) {
			$instance = new $classname();
			self::set( $classname, $instance );
		}

		return $instance;
	}

}
