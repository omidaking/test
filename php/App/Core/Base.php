<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Core;

/**
 * Base Registry Trait
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Core
 */
trait Base {

	/**
	 * Variable that holds all objects in registry.
	 *
	 * @var array
	 */
	protected static $stored_objects = [];

	/**
	 * Add object to registry
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	public static function set( $key, $value ) {
		if ( ! is_string( $key ) ) {
			_doing_it_wrong( esc_html__( 'Key passed to `set` method must be key', 'block-scaffolding-wp-omid' ), '1.0.0' );
		}

		static::$stored_objects[ $key ] = $value;

	}

	/**
	 * Get object from registry
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	public static function get( $key ) {
		if ( ! is_string( $key ) ) {
			_doing_it_wrong( esc_html__( 'Key passed to `get` method must be key', 'block-scaffolding-wp-omid' ), '1.0.0' );
		}

		if ( ! isset( static::$stored_objects[ $key ] ) ) {
			return null;
		}

		return static::$stored_objects[ $key ];
	}

	/**
	 * Returns all objects
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	public static function get_all_objects() {
		return static::$stored_objects;
	}
}
