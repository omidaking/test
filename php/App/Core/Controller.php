<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Core;

/**
 * Abstract class to define/implement base methods for all controller classes
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Core
 */
trait Controller {

	use Base;

	/**
	 * Plugin interface.
	 *
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * Holds Model object.
	 *
	 * @var Object
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected $model;

	/**
	 * Holds View Object.
	 *
	 * @var Object
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected $view;

	/**
	 * Provides access to a single instance of a module using the singleton pattern.
	 *
	 * @return object
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	public static function get_instance( $plugin_interface, $model_class_name = false, $view_class_name = false ) {
		$classname       = get_called_class();
		$key_in_registry = self::get_key( $plugin_interface, $classname, $model_class_name, $view_class_name );

		$instance = self::get( $key_in_registry );

		// Create a object if no object is found.
		if ( null === $instance ) {

			// Decide model to be passed to the constructor.
			if ( false !== $model_class_name && ! ( $model_class_name instanceof \stdClass ) ) {
				$model = $model_class_name::get_instance();
			} else {
				$model = new \stdClass();
			}

			// Decide view to be passed to the constructor.
			if ( false !== $view_class_name && ! ( $view_class_name instanceof \stdClass ) ) {
				$view = new $view_class_name();
			} else {
				$view = new \stdClass();
			}

			$instance = new $classname( $plugin_interface, $model, $view );

			self::set( $key_in_registry, $instance );
		}

		return $instance;
	}

	/**
	 * Returns key used to store a particular Controller Object
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 * @return string
	 */
	public static function get_key( $plugin_interface, $controller_class_name, $model_class_name, $view_class_name ) {
		$plugin_interface = is_object( $plugin_interface ) ? get_class( $plugin_interface ) : $plugin_interface;
		$model_class_name = is_object( $model_class_name ) ? get_class( $model_class_name ) : $model_class_name;
		$view_class_name  = is_object( $view_class_name ) ? get_class( $view_class_name ) : $view_class_name;
		return "{$plugin_interface}__{$controller_class_name}__{$model_class_name}__{$view_class_name}";
	}

	/**
	 * Get plugin.
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected function get_plugin() {
		return $this->plugin;
	}

	/**
	 * Get model.
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected function get_model() {
		return $this->model;
	}

	/**
	 * Get view
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected function get_view() {
		return $this->view;
	}

	/**
	 * Sets plugin
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected function set_plugin( $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 * Sets the model to be used
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected function set_model( $model ) {
		$this->model = $model;
	}

	/**
	 * Sets the view to be used
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected function set_view( $view ) {
		$this->view = $view;
	}

	/**
	 * Constructor
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 */
	protected function __construct( $plugin, $model, $view = false ) {
		$this->_init( $plugin, $model, $view );
	}

	/**
	 * Sets Model & View to be used with current controller
	 *
	 * @since      1.0.0
	 * @package    BlockScaffolding
	 * @return void
	 */
	final protected function _init( $plugin, $model, $view = false ) {
		$this->set_plugin( $plugin );
		$this->set_model( $model );

		if ( false === $view ) {
			$view = new \stdClass();
		}

		$this->set_view( $view );
	}

}
