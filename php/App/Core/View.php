<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Core;

use BlockScaffolding;

/**
 * Class responsible for loading templates.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Core
 */
class View {

	/**
	 * Render Templates
	 *
	 * @access public
	 * @return void
	 */
	public static function render_template( $template_name, $variables = array(), $require = 'always', $_use_variables = false ) {
		$template_path = $template_name;
		// Check if the filter is registered hook into it
		$filter_name = 'blockscaffolding/template/path/' . basename( $template_name );

		if ( $_use_variables ) {
			add_filter(
				$filter_name,
				function() use ( $template_path, $variables, $_use_variables ) {
					return array(
						'template_path'  => $template_path,
						'variables'      => $variables,
						'_use_variables' => $_use_variables,
					);
				}
			);

			return;
		}

		extract( $variables );

		ob_start();

		if ( 'always' == $require ) {
			require( $template_path );
		} else {
			require_once( $template_path );
		}

		return ob_get_clean();
	}

}
