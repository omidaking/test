<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Components\Blockeditor;

use XWP\BlockScaffolding\App\Controllers\Block;
use XWP\BlockScaffolding\App\Interfaces\Renderable;
use XWP\BlockScaffolding\App\Traits\AMPStatistics as AMPStatisticsItems;

/**
 * Class AMPStatistics
 *
 * @since  1.0.0
 * @package XWP\BlockEditor
 */
class AMPStatistics extends Block implements Renderable {

	use AMPStatisticsItems;

	/**
	 * The name of the block.
	 *
	 * @var string
	 * @since  1.0.0
	 */
	protected $block_name = 'ampstatistics';

	/**
	 * Register the script.
	 *
	 * @since  1.0.0
	 */
	public function register_script() {
		wp_enqueue_script(
			"{$this->get_block_asset_prefix()}script",
			$this->plugin->asset_url( 'assets/js/dist/block/ampstatistics/editor.js' ),
			[
				'wp-editor',
				'wp-blocks',
				'wp-api-fetch',
				'wp-i18n',
			],
			$this->plugin->asset_version(),
			true
		);
	}

	/**
	 * Get a set of attributes for ampstatistics block.
	 *
	 * @return array List of block attributes
	 */
	protected function attributes() {
		return array(
			'showAMPAdditionalStatistics' => array(
				'type'    => 'boolean',
				'default' => true,
			),
		);
	}

	/**
	 * Main render function.
	 *
	 * @since  1.0.0
	 */
	public function render( $attributes, $content ) {
		return $this->get_view()->render_amp_statistics(
			$this->plugin->template_path(),
			array(
				'statistics'                  => $this->get_items( false ),
				'showAMPAdditionalStatistics' => $attributes['showAMPAdditionalStatistics'] ? $attributes['showAMPAdditionalStatistics'] : false,
			)
		);
	}

}
