<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Interfaces;

/**
 * Interface for objects that need to hook themselves.
 *
 * @package XWP\Interfaces
 * @since   1.0.0
 */
interface Hookable {
	/**
	 * Register actions and filters with WordPress.
	 *
	 * @since  1.0.0
	 */
	public function register_hooks();
}
