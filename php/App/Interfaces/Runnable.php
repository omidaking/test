<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Interfaces;

/**
 * Interface Runnable
 *
 * @package XWP\Interfaces
 * @since   1.0.0
 */
interface Runnable {
	/**
	 * Run the initialization process.
	 *
	 * @since  1.0.0
	 */
	public function run();
}
