<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Interfaces;

/**
 * RouteInterface.
 *
 * @internal This API is used internally by Blocks.
 */
interface RouteInterface {
	/**
	 * Get the namespace for this route.
	 *
	 * @return string
	 */
	public function get_namespace();

}
