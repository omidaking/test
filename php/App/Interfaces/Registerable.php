<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Interfaces;

/**
 * Interface for objects that need to register themselves with WordPress.
 *
 * @package XWP\Interfaces
 * @since   1.0.0
 */
interface Registerable {
	/**
	 * Register this object with WordPress.
	 *
	 * @since  1.0.0
	 */
	public function register();
}
