<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Interfaces;

/**
 * Define a contract for an object that can be rendered.
 *
 * @package XWP\Interfaces
 * @since  1.0.0
 */
interface Renderable {
	/**
	 * Render a value from the object.
	 *
	 * @since  1.0.0
	 */
	public function render( $attributes, $content );
}
