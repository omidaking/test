<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Interfaces;

use XWP\BlockScaffolding\App\Interfaces\Registerable;

/**
 * Interface for objects that need to register themselves with WordPress.
 *
 * @package XWP\Interfaces
 * @since   1.0.0
 */
interface Editorblockinterface extends Registerable {

}
