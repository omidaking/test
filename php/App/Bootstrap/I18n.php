<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Bootstrap;

/**
 * Define the internationalization functionality.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Bootstrap
 */
class I18n {

	/**
	 * The domain specified for this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $domain
	 */
	private $domain;

	/**
	 * The path specified for this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $path
	 */
	private $path;

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain(
			$this->domain,
			false,
			$this->path . '/languages/'
		);
	}

	/**
	 * Set the domain.
	 *
	 * @since    1.0.0
	 * @param    string $domain
	 */
	public function set_domain( $domain ) {
		$this->domain = $domain;
	}

	/**
	 * Set the path.
	 *
	 * @since    1.0.0
	 * @param    string $path
	 */
	public function set_path( $path ) {
		$this->path = $path;
	}

}
