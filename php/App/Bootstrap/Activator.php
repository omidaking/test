<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Bootstrap;

/**
 * Fired during plugin activation.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Bootstrap
 */
class Activator {

	/**
	 * Activate method.
	 *
	 * @since    1.0.0
	 */
	public function activate() {}

}
