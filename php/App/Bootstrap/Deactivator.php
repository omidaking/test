<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Bootstrap;

/**
 * Fired during plugin deactivation.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Bootstrap
 */
class Deactivator {

	/**
	 * Deactivate method.
	 *
	 * @since    1.0.0
	 */
	public function deactivate() {}

}
