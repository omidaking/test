<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Controllers;

use XWP\BlockScaffolding\App\Core\Controller;
use XWP\BlockScaffolding\App\Services\AMPStatisticsRestController;
use XWP\BlockScaffolding\App\Interfaces\Runnable;
use XWP\BlockScaffolding\App\Interfaces\Hookable;
use XWP\BlockScaffolding\App\Interfaces\RouteInterface;
use WP_REST_Controller;

/**
 * Base REST registration controller.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Controllers
 */
class BaseRestController extends WP_REST_Controller implements Runnable, Hookable, RouteInterface {

	use Controller;

	protected $namespace = 'block-scaffolding/v1';

	/**
	 * Editor blocks registered by this service.
	 *
	 * @var array
	 * @since  1.0.0
	 */
	private $routes = [
		AMPStatisticsRestController::class,
	];

	/**
	 * Run the initialization process.
	 *
	 * @since  1.0.0
	 */
	public function run() {
		$this->register_hooks();
	}

	/**
	 * Get the namespace for this route.
	 *
	 * @return string
	 */
	public function get_namespace() {
		return $this->namespace;
	}

	/**
	 * Register this service with WordPress.
	 *
	 * @since  1.0.0
	 */
	public function register_hooks() {
		add_action( 'rest_api_init', [ $this, 'register_route' ] );
	}

	/**
	 * Hook callback to register blocks with the editor.
	 *
	 * @since  1.0.0
	 */
	public function register_route() {
		foreach ( $this->routes as $route_class ) {
			$route = $route_class::get_instance( $this->get_plugin(), $this->get_model(), $this->get_view() );
			$route->register();
		}
	}

}
