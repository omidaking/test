<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Controllers;

use XWP\BlockScaffolding\App\Core\Controller;

/**
 * Abstract for block related Controllers.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Controllers
 */
abstract class BaseBlockController {

	use Controller;

	/**
	 * Block namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'block-scaffolding';


	/**
	 * Block name within this namespace.
	 *
	 * @var string
	 */
	protected $block_name = '';

	/**
	 * Register the block with WordPress.
	 *
	 * @since  1.0.0
	 */
	public function register() {
		$this->register_script();
		$this->register_block_type();
	}

	/**
	 * Helper method to convert the block name to an asset prefix.
	 *
	 * @since  1.0.0
	 * @return string
	 */
	protected function get_block_asset_prefix() {
		return str_replace( '/', '-', $this->block_name ) . '-';
	}

	/**
	 * Helper method to convert the block name to an asset prefix.
	 *
	 * @since  1.0.0
	 * @return string
	 */
	protected function editor_script() {
		return $this->get_block_asset_prefix() . 'script';
	}

	/**
	 * Registers the block type with WordPress.
	 */
	protected function register_block_type() {
		$block_settings = [
			'attributes'      => $this->attributes(),
			'editor_script'   => $this->editor_script(),
			'render_callback' => $this->get_render_callback(),
		];

		register_block_type(
			$this->get_block_type(),
			$block_settings
		);
	}

	/**
	 * Get the block type.
	 *
	 * @return string
	 */
	protected function get_block_type() {
		return $this->namespace . '/' . $this->block_name;
	}

	/**
	 * Get the render callback for this block type.
	 *
	 * @see $this->register_block_type()
	 * @return callable|null;
	 */
	protected function get_render_callback() {
		return [ $this, 'render' ];
	}


	/**
	 * Register the JavaScript assets that power the block.
	 *
	 * @since  1.0.0
	 */
	abstract public function register_script();


	/**
	 * Get a set of attributes shared across most of the grid blocks.
	 *
	 * @return array List of block attributes with type and defaults.
	 */
	protected function attributes() {
		return [];
	}

	/**
	 * Render the block. Extended by children.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content.
	 * @return string Rendered block type output.
	 */
	public function render( $attributes, $content ) {}

}
