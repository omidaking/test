<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Controllers;

use XWP\BlockScaffolding\App\Controllers\BaseBlockController;
use XWP\BlockScaffolding\App\Interfaces\Editorblockinterface;
use XWP\BlockScaffolding\App\Interfaces\Runnable;
use XWP\BlockScaffolding\App\Interfaces\Hookable;
use XWP\BlockScaffolding\App\Components\Blockeditor\AMPStatistics;

/**
 * Block registration class.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Controller
 */
class Block extends BaseBlockController implements Runnable, Hookable, Editorblockinterface {

	/**
	 * Editor blocks registered by this service.
	 *
	 * @var array
	 * @since  1.0.0
	 */
	private $blocks = [
		AMPStatistics::class,
	];

	/**
	 * Run the initialization process.
	 *
	 * @since  1.0.0
	 */
	public function run() {
		$this->register_hooks();
	}

	/**
	 * Register this service with WordPress.
	 *
	 * @since  1.0.0
	 */
	public function register_hooks() {
		add_action( 'init', [ $this, 'register_blocks' ] );
	}

	/**
	 * Hook callback to register blocks with the editor.
	 *
	 * @since  1.0.0
	 */
	public function register_blocks() {
		foreach ( $this->blocks as $block_class ) {
			$block = $block_class::get_instance( $this->get_plugin(), $this->get_model(), $this->get_view() );
			$this->register_block( $block );
		}
	}

	/**
	 * Register an editor block with WordPress.
	 *
	 * @param Editorblockinterface $block An object instance.
	 * @since  1.0.0
	 */
	private function register_block( Editorblockinterface $block ) {
		$block->register();
	}

	/**
	 * Register the JavaScript assets that power the block.
	 *
	 * @since  1.0.0
	 */
	public function register_script(){}

}
