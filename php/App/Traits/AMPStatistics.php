<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Traits;

use AMP_Options_Manager;
use AMP_Validated_URL_Post_Type;
use AMP_Validation_Error_Taxonomy;
use WP_REST_Request;

/**
 * Method to share fetching items between route and block.
 *
 * @since      1.0.0
 * @package    BlockScaffolding
 * @subpackage BlockScaffolding/Traits
 */
trait AMPStatistics {
	/**
	 * Retrieves total unreviewed count for validation URLs and errors.
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_REST_Response|WP_Error Response object on success, or WP_Error object on failure.
	 * @see ValidationCountsRestController class in AMP plugin.
	 */
	public function get_items( $request = false ) { // phpcs:ignore VariableAnalysis.CodeAnalysis.VariableAnalysis.UnusedVariable
		$unreviewed_validated_url_count    = AMP_Validated_URL_Post_Type::get_validation_error_urls_count();
		$unreviewed_validation_error_count = AMP_Validation_Error_Taxonomy::get_validation_error_count(
			[
				'group' => [
					AMP_Validation_Error_Taxonomy::VALIDATION_ERROR_NEW_REJECTED_STATUS,
					AMP_Validation_Error_Taxonomy::VALIDATION_ERROR_NEW_ACCEPTED_STATUS,
				],
			]
		);

		$response = [
			'validated_urls' => $unreviewed_validated_url_count,
			'errors'         => $unreviewed_validation_error_count,
			'theme_support'  => AMP_Options_Manager::get_option( 'theme_support', '' ),
		];

		if ( $request instanceof WP_REST_Request ) {
			return rest_ensure_response( $response );
		}

		return $response;
	}
}
