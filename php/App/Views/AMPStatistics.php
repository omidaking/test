<?php
/**
 * @link       https://www.xwp.co
 * @since      1.0.0
 *
 * @package    BlockScaffolding
 */
namespace XWP\BlockScaffolding\App\Views;

use XWP\BlockScaffolding\App\Core\View;

class AMPStatistics extends View {

	/**
	 * Prints contents of block.
	 *
	 * @param  array $args
	 * @return void
	 * @since 1.0.0
	 */
	public function render_amp_statistics( $path, $args = [] ) {
		return $this->render_template(
			$path . 'Blocks/AmpStatistics.php',
			$args
		);
	}

}
