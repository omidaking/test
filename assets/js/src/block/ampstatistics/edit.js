/**
 * External dependencies
 */
import { sprintf, __ } from "@wordpress/i18n";
import { InspectorControls } from "@wordpress/block-editor";
import apiFetch from "@wordpress/api-fetch";
import { PanelBody, ToggleControl } from "@wordpress/components";
/**
 * WordPress dependencies
 */
import { createContext, useEffect, useRef, useState } from "@wordpress/element";
export const Options = createContext();

const Edit = ({ attributes, setAttributes }) => {
	const { showAMPAdditionalStatistics, className } = attributes;
	const [options, setOptions] = useState({
		validated_urls: 0,
		errors: 0,
		theme_support: "",
	});
	const [fetchingOptions, setFetchingOptions] = useState(null);

	/**
	 * This component sets state inside async functions.
	 * Use this ref to prevent state updates after unmount.
	 */
	const hasUnmounted = useRef(false);
	useEffect(
		() => () => {
			hasUnmounted.current = true;
		},
		{}
	);

	/**
	 * Fetches the option data.
	 */
	useEffect(() => {
		if (options.theme_support !== "" || fetchingOptions) {
			return;
		}

		(async () => {
			setFetchingOptions(true);

			try {
				const fetchedOptions = await apiFetch({
					path: "block-scaffolding/v1/amp-statistics",
				});

				if (hasUnmounted.current === true) {
					return;
				}

				setOptions(fetchedOptions);
			} catch (e) {
				return;
			}

			setFetchingOptions(false);
		})();
	}, [fetchingOptions, options]);

	const AmpInspectorControls = () => {
		return (
			<InspectorControls>
				<PanelBody
					title={ __("Aditional Statistics", "block-scaffolding-wp-omid") }
				>
					<ToggleControl
						label={ __("Display AMP template mode", "block-scaffolding-wp-omid") }
						checked={ showAMPAdditionalStatistics }
						onChange={ () =>
							setAttributes({
								showAMPAdditionalStatistics: !showAMPAdditionalStatistics,
							})
						}
					/>
				</PanelBody>
			</InspectorControls>
		);
	};

	return (
		<div className={ className }>
			<AmpInspectorControls />
			<h3> { __("AMP Validation Statistics", "block-scaffolding-wp-omid") } </h3>
			<ul>
				<li>
					{ sprintf(
						__("There are %d validated URLS", "block-scaffolding-wp-omid"),
						options.validated_urls
					) }
				</li>
				<li>
					{ sprintf(
						__("There are %d validated Errors", "block-scaffolding-wp-omid"),
						options.errors
					) }
				</li>

				{ showAMPAdditionalStatistics && (
					<li>
						{ sprintf(
							__("The template mode is %s", "block-scaffolding-wp-omid"),
							options.theme_support
						) }
					</li>
				) }
			</ul>
		</div>
	);
};

export default Edit;
