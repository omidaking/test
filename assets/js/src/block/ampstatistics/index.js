/**
 * External dependencies
 */
import { __ } from "@wordpress/i18n";
import { registerBlockType } from "@wordpress/blocks";

/**
 * Internal dependencies
 */
import Edit from "./edit";

registerBlockType("block-scaffolding/ampstatistics", {
	title: __("AMP Statistics", "block-scaffolding-wp-omid"),
	icon: "smiley",
	category: "common",
	keywords: [
		__("Statistics", "block-scaffolding-wp-omid"),
		__("AMP", "block-scaffolding-wp-omid"),
		__("amp statistics", "block-scaffolding-wp-omid"),
	],
	description: __("AMP url statistics", "block-scaffolding-wp-omid"),
	supports: {
		align: ["wide", "full"],
		html: false,
	},
	edit: Edit,
	save() {
		return null;
	},
});
